def ballarray = []
def process(balls):
	ballarray = balls.split(" ")
	batsman = battingteam.pop()
	nonstriker = battingteam.pop()
	i = 0
	runsperover = 0
	totalruns = 0
	totalwickets = 0
	wicketsperover = 0
	for b in ballarray:
		if i%6 == 0:
			batsman, nonstriker = nonstriker, batsman
			overplayed.push(runsperover,wicketsperover,bowler)
			bowler.overbowled.push(i/6)
			bowler.wickets += wicketsperover
			runsperover = 0
			wicketperover = 0
			bowler = random.choice(bowlers)
		if b == "W":
			ballsbowled.push(ball(score,0,true))
			battingteamfinal.push(batsman)
			fallofwickets(fallofwicket(batsman.name,bowler.name,totalruns,score.totalwickets+1))
			batsman = battingteam.pop()
			score = score(batsman,nonstriker,totalruns,totalwickets)
			wicketsperover += 1
			totalwickets += 1 
		else:
			ballsbowled.push(ball(score,b,false))
			batsman.runsmade += b
			batsman.ballsfaced += 1
			if b%2 != 0:
				batsman, nonstriker = nonstriker, batsman
			
			