import random
class Ball:
	def __init__(self,score,runs,wicket):
		self.score = score
		self.runs = runs
		self.wicket = wicket
	def printBall(self):
		self.score.printScore()
		print self.runs,self.wicket
		
class Over:
	def __init__(self,runs,wickets,bowlerIndex):
		self.runs = runs
		self.wickets = wickets
		self.bowlerIndex = bowlerIndex

class FallOfWicket:
	def __init__(self,batsmanIndex,bowlerIndex,totalRuns,totalBalls):
		self.batsmanIndex = batsmanIndex
		self.bowlerIndex = bowlerIndex
		self.totalRuns = totalRuns
		self.totalBalls = totalBalls
		
class Score:
	def __init__(self,batsman,nonstriker,totalRuns,totalWickets):
		self.batsman = Batsman(batsman.name,batsman.runsMade,batsman.ballsFaced)
		self.nonstriker = Batsman(nonstriker.name,nonstriker.runsMade,nonstriker.ballsFaced)
		self.totalRuns = totalRuns
		self.totalWickets = totalWickets
	def printscore(self):
		self.batsman.printbatsman()
		self.nonstriker.printbatsman()
		print self.totalRuns,self.totalWickets
		
class Batsman:
	def __init__(self,name,runsMade,ballsFaced):
		self.name = name
		self.runsMade = runsMade
		self.ballsFaced = ballsFaced
	def printbatsman(self):
		print self.name,self.runsMade,self.ballsFaced
		
class Bowler:
	def __init__(self,name,oversBowled,wickets):
		self.name = name
		self.oversBowled = oversBowled
		self.wickets = wickets

ballarray = []
battingTeam = []
bowlingTeam = []
ballsbowled = []
totalOvers = []
fallofwickets = [] 

for i in range(0,11):
	battingTeam.append(Batsman(i,0,0))
	
for i in range(0,6):
	bowlingTeam.append(Bowler(i,[],0))

def process(balls):
	count = 0
	ballarray = balls.split(" ")
	batsman = battingTeam[count]
	count += 1
	nonstriker = battingTeam[count]
	count += 1
	i = 0
	totalRuns = 0
	totalwickets = 0
	for b in ballarray:
		if i%6 == 0: 
			bowler = random.choice(bowlingTeam)
			over = Over(0,0,bowlingTeam.index(bowler))
			if i>0:
				batsman, nonstriker = nonstriker, batsman
			totalOvers.append(over)
			bowler.oversBowled.append((i/6)+1)
		if b == "W":
			batsman.ballsFaced += 1
			totalwickets += 1 
			fallofwickets.append(FallOfWicket(battingTeam.index(batsman),bowlingTeam.index(bowler),totalRuns,i+1))
			score = Score(batsman,nonstriker,totalRuns,totalwickets)
			ballsbowled.append(Ball(score,0,True))
			batsman = battingTeam[count]
			count += 1
			bowler.wickets += 1
			over.wickets += 1
		else:
			batsman.runsMade += int(b)
			batsman.ballsFaced += 1
			totalRuns += int(b)
			over.runs += int(b)
			score = Score(batsman,nonstriker,totalRuns,totalwickets)
			ballsbowled.append(Ball(score,int(b),False))
			if int(b)%2 != 0:
				batsman, nonstriker = nonstriker, batsman
		i += 1

def currentBowlerSnapShot(bowlerIndex,overs,ball):
	oversDel = 0
	wicketsTaken = 0
	runsGiven = 0
	for i in range(0,overs):
		if totalOvers[i].bowlerIndex == bowlerIndex:
			oversDel += 1
			wicketsTaken += totalOvers[i].wickets
			runsGiven += totalOvers[i].runs
	for i in range(overs*6,(overs*6)+ball):
		runsGiven += ballsbowled[i].runs
		if ballsbowled[i].wicket:
			wicketsTaken += 1
	print " Overdelivered : ",int(oversDel),".",int(ball)," WicketsTaken : ",wicketsTaken
			
def output(snapshotball):
	overs,ball = snapshotball.split(".");
	ballindex = int(overs)*6+int(ball)-1
	print "Total Score: ",ballsbowled[ballindex].score.totalRuns,"/",ballsbowled[ballindex].score.totalWickets
	print "Batsman ",ballsbowled[ballindex].score.batsman.name,"* : RunsMade-",ballsbowled[ballindex].score.batsman.runsMade," BallsFaced-",ballsbowled[ballindex].score.batsman.ballsFaced
	print "Batsman ",ballsbowled[ballindex].score.nonstriker.name," : RunsMade-",ballsbowled[ballindex].score.nonstriker.runsMade," BallsFaced-",ballsbowled[ballindex].score.nonstriker.ballsFaced
	print "Bowlername :",bowlingTeam[totalOvers[int(overs)].bowlerIndex].name,currentBowlerSnapShot(totalOvers[int(overs)].bowlerIndex,int(overs),int(ball))
	
balls = "1 2 2 W 3 4 1 2 3 4 5 W"
process(balls)
output("1.2")